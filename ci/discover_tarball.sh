#! /usr/bin/env bash

set -e

cd "$(dirname "${BASH_SOURCE[0]}")"

ARCH="$(uname -m)"

>&2 echo "NIGHTLY=${NIGHTLY}"
>&2 echo "RELEASE_JOB=${RELEASE_JOB}"
>&2 echo "SLOW_VALIDATE=${SLOW_VALIDATE}"
>&2 echo "ARCH=${ARCH}"

# NOTE: If you add a new JOB_NAME here then you also might need to modify the
# `needs` field in ghc/ghc .gitlab-ci.yml to avoid triggering the downstream job
# too early.

case $ARCH in
  aarch64)
    if [ -n "$NIGHTLY" ]; then
      BINDIST_NAME="ghc-aarch64-linux-deb10-validate.tar.xz"
      JOB_NAME="nightly-aarch64-linux-deb10-validate"
    elif [ "$RELEASE_JOB" == "yes" ]; then
      BINDIST_NAME="ghc-aarch64-linux-deb10-release+no_split_sections.tar.xz"
      JOB_NAME="release-aarch64-linux-deb10-release+no_split_sections"
    else
      BINDIST_NAME="ghc-aarch64-linux-deb10-validate.tar.xz"
      JOB_NAME="aarch64-linux-deb10-validate"
    fi
  ;;
  *)
    if [ -n "$SLOW_VALIDATE" ]; then
      BINDIST_NAME="ghc-x86_64-linux-deb10-numa-slow-validate.tar.xz"
      if [ -n "$NIGHTLY" ]; then
        JOB_NAME="nightly-x86_64-linux-deb10-numa-slow-validate"
      elif [ "$RELEASE_JOB" == "yes" ]; then
        echo "No slow validate build in release job"
        exit 2
      else
        JOB_NAME="x86_64-linux-deb10-numa-slow-validate"
      fi
    else
      BINDIST_NAME="ghc-x86_64-linux-fedora33-release.tar.xz"
      if [ -n "$NIGHTLY" ]; then
        JOB_NAME="nightly-x86_64-linux-fedora33-release"
      elif [ "$RELEASE_JOB" == "yes" ]; then
        JOB_NAME="release-x86_64-linux-fedora33-release"
      else
        JOB_NAME="x86_64-linux-fedora33-release"
      fi
    fi
  ;;
esac

>&2 echo "BINDIST_NAME=${BINDIST_NAME}"
>&2 echo "JOB_NAME=${JOB_NAME}"

if [ -n "$UPSTREAM_COMMIT_SHA" ]; then
  # N.B. We can't use this if the upstream pipeline might be in-progress
  # since the below URL cannot provide an artifact until a pipeline has
  # run to completion on the requested branch. This is in general
  # not the case for GHC pipelines. Consequently, in this case we will
  # usually rather provide UPSTREAM_PIPELINE_ID.
  >&2 echo "Pulling binary distribution from commit $UPSTREAM_COMMIT_SHA of project $UPSTREAM_PROJECT_PATH..."
  GHC_TARBALL="https://gitlab.haskell.org/$UPSTREAM_PROJECT_PATH/-/jobs/artifacts/$UPSTREAM_COMMIT_SHA/raw/$BINDIST_NAME?job=$JOB_NAME"
elif [ -n "$UPSTREAM_PIPELINE_ID" ]; then
  job_name=$JOB_NAME
  >&2 echo "Pulling ${job_name} binary distribution from Pipeline $UPSTREAM_PIPELINE_ID..."
  job_id=$(find-job $UPSTREAM_PROJECT_ID $UPSTREAM_PIPELINE_ID $job_name)
  >&2 echo "Using job $job_id..."
  echo "https://gitlab.haskell.org/$UPSTREAM_PROJECT_PATH/-/jobs/$job_id/artifacts/raw/$BINDIST_NAME"
elif [ -n "$UPSTREAM_BRANCH_NAME" ]; then
  job_name=$JOB_NAME
  >&2 echo "Finding ${job_name} binary distribution from $UPSTREAM_BRANCH_NAME..."
  job_id=$(find-latest-job "$UPSTREAM_PROJECT_ID" "$UPSTREAM_BRANCH_NAME" "$JOB_NAME")
  >&2 echo "Using job $job_id..."
  echo "https://gitlab.haskell.org/$UPSTREAM_PROJECT_PATH/-/jobs/$job_id/artifacts/raw/$BINDIST_NAME"
fi
