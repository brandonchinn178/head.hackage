{ pkgs }:

# Maps Haskell package names to a list of the Nixpkgs attributes corresponding
# to their native library dependencies.
with pkgs;
{
  zlib = [ zlib ];
  digest = [ zlib ];
  regex-pcre = [ pcre ];
  bzlib = [ bzip2 ];
  hmatrix = [ blas liblapack ];
  hexpat = [ expat ];
  hgmp = [ gmp ];
  posix-api = [ systemd ];
  lame = [ lame ];
}
